package com.builder.clientes.port.adapter.repository;

import com.builder.clientes.application.Consulta;
import com.builder.clientes.domain.model.Cliente;
import com.builder.clientes.domain.model.ClienteRepository;
import com.builder.clientes.resource.dto.ClienteResource;
import com.builder.clientes.resource.translate.ClienteTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import java.util.Optional;

public class ClienteRepositoryImpl implements ClienteRepository {

    @Autowired
    private ClienteJpaRepository clienteJpagRepository;

    @Autowired
    private ClientePageRepository clientePageRepository;

    public ClienteRepositoryImpl(ClienteJpaRepository clienteJpagRepository, ClientePageRepository clientePageRepository) {
        this.clienteJpagRepository = clienteJpagRepository;
        this.clientePageRepository = clientePageRepository;
    }

    @Override
    public Optional<Cliente> salvar(@NotNull Cliente cliente) {
        return Optional.ofNullable(clienteJpagRepository.save(cliente));
    }

    @Override
    public Optional<Cliente> atualizar(@NotNull Cliente cliente) {
        return Optional.ofNullable(clienteJpagRepository.save(cliente));
    }

    @Override
    public Optional<Cliente> atualizarNomeCliente(@NotNull Long id, String nome) throws DatatypeConfigurationException {
        Cliente cliente = clienteJpagRepository.findById(id).get();
        ClienteResource clienteResource = new ClienteResource();
        clienteResource.setNome(nome);
        clienteResource.setCpf(cliente.getCPF());
        clienteResource.setDataNascimento(cliente.getDataNascimento());

        return Optional.ofNullable(clienteJpagRepository.save(ClienteTranslator.paraEntidade(clienteResource, id)));
    }

    @Override
    public Optional<Cliente> pesquisarPorId(@NotNull Long id) {
        return clienteJpagRepository
                .findById(id);
    }

    @Override
    public void deletar(@NotNull Long id) {
        clienteJpagRepository.deleteById(id);
    }

    @Override
    public Page<Cliente> pesquisarClientes(@NotNull Consulta consulta, @NotNull Pageable pageable) {
        return clientePageRepository
                .findAll(new ClienteSpecification(consulta), pageable);
    }

}
