package com.builder.clientes.port.adapter.configuration;

import com.builder.clientes.domain.model.ClienteRepository;
import com.builder.clientes.port.adapter.repository.ClienteJpaRepository;
import com.builder.clientes.port.adapter.repository.ClientePageRepository;
import com.builder.clientes.port.adapter.repository.ClienteRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Beans {

    @Bean
    public ClienteRepository clienteRepository(ClienteJpaRepository clienteJpaRepository, ClientePageRepository clientePageRepository){
        return new ClienteRepositoryImpl(clienteJpaRepository, clientePageRepository);
    }

}
