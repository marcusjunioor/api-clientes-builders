package com.builder.clientes.port.adapter.repository;

import com.builder.clientes.domain.model.Cliente;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientePageRepository extends PagingAndSortingRepository<Cliente, Long>,
        JpaSpecificationExecutor<Cliente> {
}
