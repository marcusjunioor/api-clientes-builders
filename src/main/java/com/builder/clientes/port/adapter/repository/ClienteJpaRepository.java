package com.builder.clientes.port.adapter.repository;

import com.builder.clientes.domain.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteJpaRepository
        extends JpaRepository<Cliente, Long>, JpaSpecificationExecutor<Cliente> {

}
