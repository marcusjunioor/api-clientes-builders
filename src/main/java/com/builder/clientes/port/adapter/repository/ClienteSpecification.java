package com.builder.clientes.port.adapter.repository;

import com.builder.clientes.application.Consulta;
import com.builder.clientes.domain.model.Cliente;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ClienteSpecification implements Specification<Cliente> {

    private final Consulta consulta;

    public ClienteSpecification(Consulta query) {
        this.consulta = query;
    }

    private Consulta getConsulta() {
        return Objects.requireNonNullElse(this.consulta, new Consulta());
    }

    @Override
    public Predicate toPredicate(Root<Cliente> root,
                                 CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder) {

        final Consulta queryEnt = getConsulta();
        final List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.isNotBlank(queryEnt.getNome())) {
            predicates.add(criteriaBuilder.like(root.get("nome"), "%" + queryEnt.getNome() + "%"));
        }

        if (StringUtils.isNotBlank(queryEnt.getCpf())) {
            predicates.add(root.get("CPF").in(queryEnt.getCpf()));
        }

        if(predicates.isEmpty()){
            predicates.add(criteriaBuilder.like(root.get("nome"), "%" + "" + "%"));;
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[1]));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClienteSpecification that = (ClienteSpecification) o;
        return Objects.equals(consulta, that.consulta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consulta);
    }
}
