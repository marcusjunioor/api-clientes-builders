package com.builder.clientes.resource.translate;

import com.builder.clientes.domain.model.Cliente;
import com.builder.clientes.resource.dto.ClienteDTO;
import com.builder.clientes.resource.dto.ClienteResource;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import java.time.LocalDate;
import java.time.Period;

public class ClienteTranslator {

    public static Cliente paraModelo(@NotNull final ClienteResource from) throws DatatypeConfigurationException {

        return Cliente.builder()
                .nome(from.getNome())
                .CPF(from.getCpf())
                .dataNascimento(from.getDataNascimento())
                .build();
    }

    public static Cliente paraEntidade(@NotNull final ClienteResource from, @NotNull Long id) throws DatatypeConfigurationException {

        return Cliente.builder()
                .id(id)
                .nome(from.getNome())
                .CPF(from.getCpf())
                .dataNascimento(from.getDataNascimento())
                .build();
    }

    public static ClienteDTO paraDTO(@NotNull final Cliente from){

        Integer idadeDoCliente = Period.between(from.getDataNascimento(), LocalDate.now()).getYears();

        return ClienteDTO.builder()
                .id(from.getId())
                .nome(from.getNome())
                .CPF(from.getCPF())
                .dataNascimento(from.getDataNascimento())
                .idade(idadeDoCliente.longValue())
                .build();
    }
}
