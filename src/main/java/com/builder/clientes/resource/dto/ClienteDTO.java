package com.builder.clientes.resource.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@Builder
public class ClienteDTO {

    private Long id;
    private String nome;
    private String CPF;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dataNascimento;

    private Long idade;

}
