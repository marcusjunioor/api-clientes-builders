package com.builder.clientes.resource.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ClienteResource {

    private String nome;
    private String cpf;
    private LocalDate dataNascimento;

}
