package com.builder.clientes.resource;

import com.builder.clientes.application.ClienteService;
import com.builder.clientes.application.Consulta;
import com.builder.clientes.resource.dto.ClienteDTO;
import com.builder.clientes.resource.dto.ClienteResource;
import com.builder.clientes.resource.translate.ClienteTranslator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.xml.datatype.DatatypeConfigurationException;

@RestController
@RequestMapping(value = "/v1/clientes")
@Api(tags = "Clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping
    @ApiOperation(value = "Pesquisar cliente")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Clientes retornados com sucesso.", response = ClienteDTO.class),
            @ApiResponse(code = 400, message = "Requisição inválida."),
            @ApiResponse(code = 500, message = "Erro interno.")
    })
    public ResponseEntity<Page<ClienteDTO>> pesquisarPorId(
            Consulta consulta,
            @RequestParam(value = "page", required = true, defaultValue = "0") Integer page,
            @RequestParam(value = "size", required = true, defaultValue = "20") Integer size) {

        Page<ClienteDTO> paginaCLientes =
                clienteService
                        .pesquisarClientes(consulta, PageRequest.of(page, size))
                        .map(ClienteTranslator::paraDTO);

        return ResponseEntity.ok(paginaCLientes);
    }

    @PostMapping
    @ApiOperation(value = "Criar cliente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Cliente criado com sucesso.", response = ClienteResource.class),
            @ApiResponse(code = 400, message = "Requisição inválida."),
            @ApiResponse(code = 500, message = "Erro interno.")
    })
    public ResponseEntity criarCliente(@RequestBody @Validated ClienteResource clienteResource) throws DatatypeConfigurationException {

        return clienteService
                .criarCliente(ClienteTranslator.paraModelo(clienteResource))
                .map(cliente -> ResponseEntity.ok().build())
                .orElseThrow(RuntimeException::new);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Atualizar cliente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Cliente atualizada com sucesso.", response = ClienteResource.class),
            @ApiResponse(code = 400, message = "Requisição inválida."),
            @ApiResponse(code = 500, message = "Erro interno.")
    })
    public ResponseEntity atualizarCliente(@PathVariable Long id, @RequestBody @Validated ClienteResource clienteResource) throws DatatypeConfigurationException {

        return clienteService
                .atualizarCliente(ClienteTranslator.paraEntidade(clienteResource, id))
                .map(cliente -> ResponseEntity.ok().build())
                .orElseThrow(RuntimeException::new);
    }

    @PatchMapping(path = "/{id}")
    @ApiOperation(value = "Atualizar nome do cliente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Nome atualizado com sucesso.", response = ClienteResource.class),
            @ApiResponse(code = 400, message = "Requisição inválida."),
            @ApiResponse(code = 500, message = "Erro interno.")
    })
    public ResponseEntity atualizarNomeCliente(@PathVariable Long id, @RequestParam String nome) throws DatatypeConfigurationException {

        return clienteService
                .atualizarNomeCliente(id, nome)
                .map(cliente -> ResponseEntity.ok().build())
                .orElseThrow(RuntimeException::new);
    }


    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Pesquisar cliente por id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna cliente com o id informado.", response = ClienteResource.class),
            @ApiResponse(code = 400, message = "Requisição inválida."),
            @ApiResponse(code = 404, message = "Cliente não existe."),
            @ApiResponse(code = 500, message = "Erro interno.")
    })
    public ResponseEntity pesquisarPorId(@PathVariable Long id) {

        return clienteService
                .pesquisarPorId(id)
                .map(ClienteTranslator::paraDTO)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Deletar cliente")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Cliente deletado com sucesso.", response = ClienteResource.class),
            @ApiResponse(code = 400, message = "Requisição inválida."),
            @ApiResponse(code = 404, message = "Cliente não existe."),
            @ApiResponse(code = 500, message = "Erro interno.")
    })
    public ResponseEntity deletar(@PathVariable Long id) {

        clienteService.deletar(id);
        return ResponseEntity.noContent()
                .build();
    }

}
