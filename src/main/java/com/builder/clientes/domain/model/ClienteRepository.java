package com.builder.clientes.domain.model;

import com.builder.clientes.application.Consulta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import java.util.Optional;

public interface ClienteRepository {

    Optional<Cliente> salvar(@NotNull Cliente cliente);
    Optional<Cliente> atualizar(@NotNull Cliente cliente);
    Optional<Cliente> atualizarNomeCliente(@NotNull Long id, String nome) throws DatatypeConfigurationException;
    Optional<Cliente> pesquisarPorId(@NotNull Long id);
    Page<Cliente> pesquisarClientes(@NotNull Consulta consulta, @NotNull Pageable pageable);
    void deletar(@NotNull Long id);

}
