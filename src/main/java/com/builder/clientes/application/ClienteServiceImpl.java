package com.builder.clientes.application;

import com.builder.clientes.domain.model.Cliente;
import com.builder.clientes.domain.model.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import java.util.Optional;

@Service
public class ClienteServiceImpl implements  ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public ClienteServiceImpl(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @Override
    public Optional<Cliente> criarCliente(@NotNull Cliente cliente) {
        return clienteRepository.salvar(cliente);
    }

    @Override
    public Optional<Cliente> atualizarCliente(@NotNull Cliente cliente) {
        return clienteRepository.atualizar(cliente);
    }

    @Override
    public Optional<Cliente> atualizarNomeCliente(@NotNull Long id, String nome) throws DatatypeConfigurationException {
        return clienteRepository.atualizarNomeCliente(id, nome);
    }

    @Override
    public Optional<Cliente> pesquisarPorId(@NotNull Long id) {
        return clienteRepository.pesquisarPorId(id);
    }

    @Override
    public void deletar(@NotNull Long id) {
        clienteRepository.deletar(id);
    }

    @Override
    public Page<Cliente> pesquisarClientes(@NotNull Consulta consulta, @NotNull Pageable pageable) {
        return clienteRepository.pesquisarClientes(consulta, pageable);
    }
}
