package com.builder.clientes.application;

import com.builder.clientes.domain.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import javax.xml.datatype.DatatypeConfigurationException;
import java.util.Optional;

public interface ClienteService {

    Optional<Cliente> criarCliente(@NotNull Cliente cliente);
    Optional<Cliente> atualizarCliente(@NotNull Cliente cliente);
    Optional<Cliente> atualizarNomeCliente(@NotNull Long id, String nome) throws DatatypeConfigurationException;
    Optional<Cliente> pesquisarPorId(@NotNull Long id);
    void deletar(@NotNull Long id);
    Page<Cliente> pesquisarClientes(@NotNull Consulta consulta, @NotNull Pageable pageable);

}
