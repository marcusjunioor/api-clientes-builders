package com.builder.clientes.application;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Consulta {

    private String nome;
    private String cpf;

}
