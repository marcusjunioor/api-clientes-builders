# API de clientes

Está é uma API referente ao um desafio técnico para a Platform Builders.

## Instalação

Versão do Java utilizada: 11
Banco: MySQL em cloud google

```bash
mvn clean install
```

## Utilização

A uma documentação disponível via swagger no projeto,
disponível em: http://localhost:8080/swagger-ui.html

Há também um arquivo Postman exportado dentro do projeto
para apreciação conforme solicitado.

## Autor

Marcus Júnior

